package com.devsu.codejam.microserviceone;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MicroserviceOneApplication {

  @Value("${spring.microservice-one.authentication}")
  private boolean authentication;

  public static void main(String[] args) {
    SpringApplication.run(MicroserviceOneApplication.class, args);
  }

  @Bean
  @Qualifier("authentication")
  boolean provideDefaultAuthentication() {
    return authentication;
  }

}
