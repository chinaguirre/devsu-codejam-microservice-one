package com.devsu.codejam.microserviceone.representation;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SorterElementsResponse {

  private String status;
  private String message;
  private ElementsData data;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ElementsData getData() {
    return data;
  }

  public void setData(ElementsData data) {
    this.data = data;
  }
}
