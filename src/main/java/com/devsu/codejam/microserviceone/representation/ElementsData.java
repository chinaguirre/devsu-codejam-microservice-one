package com.devsu.codejam.microserviceone.representation;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ElementsData {

  private List<String> sorted;

  private List<Number> numbers;

  private List<String> others;

  public ElementsData(List<String> sorted) {
    this.sorted = sorted;
  }

  public ElementsData(List<Number> numbers, List<String> others) {
    this.numbers = numbers;
    this.others = others;
  }

  public List<Number> getNumbers() {
    return numbers;
  }

  public void setNumbers(List<Number> numbers) {
    this.numbers = numbers;
  }

  public List<String> getOthers() {
    return others;
  }

  public void setOthers(List<String> others) {
    this.others = others;
  }

  public List<String> getSorted() {
    return sorted;
  }

  public void setSorted(List<String> sorted) {
    this.sorted = sorted;
  }

}
