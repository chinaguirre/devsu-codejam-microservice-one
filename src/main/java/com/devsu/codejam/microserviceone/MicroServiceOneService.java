package com.devsu.codejam.microserviceone;

import com.devsu.codejam.microserviceone.representation.ElementsData;
import com.devsu.codejam.microserviceone.representation.SorterElementsRequest;
import com.devsu.codejam.microserviceone.representation.SorterElementsResponse;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class MicroServiceOneService {

  public SorterElementsResponse sortElementsToString(SorterElementsRequest request) {

    List<String> values = request.getElements().stream().map(String::valueOf).sorted().collect(Collectors.toList());
    SorterElementsResponse response = new SorterElementsResponse();
    response.setData(new ElementsData(values));
    response.setStatus("success");
    response.setMessage("ok");
    return response;
  }

  public SorterElementsResponse sortElementsDependingType(SorterElementsRequest request) {
    List<Number> sortedNumbers =
        request.getElements().stream().filter(e -> e instanceof Number).map(String::valueOf).map(BigDecimal::new).sorted().collect(Collectors.toList());
    List<String> sortedOtherElements =
        request.getElements().stream().filter(e -> !(e instanceof Number)).map(String::valueOf).sorted().collect(Collectors.toList());
    SorterElementsResponse response = new SorterElementsResponse();
    response.setData(new ElementsData(sortedNumbers, sortedOtherElements));
    response.setStatus("success");
    response.setMessage("ok");
    return response;
  }
}
