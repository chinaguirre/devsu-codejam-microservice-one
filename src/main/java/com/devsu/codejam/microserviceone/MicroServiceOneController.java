package com.devsu.codejam.microserviceone;

import com.devsu.codejam.microserviceone.representation.SorterElementsRequest;
import com.devsu.codejam.microserviceone.representation.SorterElementsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MicroServiceOneController implements MicroServiceOneApi {

  private final MicroServiceOneService service;

  @Autowired
  public MicroServiceOneController(MicroServiceOneService service) {
    this.service = service;
  }

  @Override
  public SorterElementsResponse sortElementsToString(SorterElementsRequest request) {
    return service.sortElementsToString(request);
  }

  @Override
  public SorterElementsResponse sortElementsDependingType(SorterElementsRequest request) {
    return service.sortElementsDependingType(request);
  }
}
