package com.devsu.codejam.microserviceone;

import com.devsu.codejam.microserviceone.representation.SorterElementsResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

  private static final String ERROR_VALUE = "error";

  @NonNull
  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> customGeneralExceptionHandleException(@NonNull Exception e, @NonNull WebRequest req) {

    SorterElementsResponse response = new SorterElementsResponse();
    response.setStatus(ERROR_VALUE);
    response.setMessage(e.getMessage());

    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @Override
  @NonNull
  public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, @NonNull HttpHeaders headers,
      @NonNull HttpStatus status, @NonNull WebRequest request) {
    SorterElementsResponse response = new SorterElementsResponse();
    response.setStatus(ERROR_VALUE);
    response.setMessage(ex.getMessage());
    return ResponseEntity
        .status(HttpStatus.BAD_REQUEST)
        .body(response);
  }

  @Override
  @NonNull
  public ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, @NonNull HttpHeaders headers,
      @NonNull HttpStatus status, @NonNull WebRequest req) {
    SorterElementsResponse response = new SorterElementsResponse();
    response.setStatus(ERROR_VALUE);
    response.setMessage(ex.getMessage());
    return ResponseEntity
        .status(HttpStatus.BAD_REQUEST)
        .body(response);
  }
}
