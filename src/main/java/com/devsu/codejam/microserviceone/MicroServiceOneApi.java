package com.devsu.codejam.microserviceone;

import com.devsu.codejam.microserviceone.representation.SorterElementsRequest;
import com.devsu.codejam.microserviceone.representation.SorterElementsResponse;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public interface MicroServiceOneApi {

  @PostMapping(value = "/element_sorter1_0"
      , produces = {"application/json"},
      consumes = {"application/json"}
  )
  @ResponseStatus(HttpStatus.OK)
  SorterElementsResponse sortElementsToString(@Valid @RequestBody SorterElementsRequest request);

  @PostMapping(value = "/element_sorter1_1"
      , produces = {"application/json"},
      consumes = {"application/json"}
  )
  @ResponseStatus(HttpStatus.OK)
  SorterElementsResponse sortElementsDependingType(@Valid @RequestBody SorterElementsRequest request);
}
