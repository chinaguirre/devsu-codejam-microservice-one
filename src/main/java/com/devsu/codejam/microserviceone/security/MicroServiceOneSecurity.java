package com.devsu.codejam.microserviceone.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
public class MicroServiceOneSecurity extends WebSecurityConfigurerAdapter {

  private static final String DEFAULT_ROLE = "ADMIN";

  private final boolean authentication;

  @Autowired
  public MicroServiceOneSecurity(@Qualifier("authentication") boolean authentication) {
    this.authentication = authentication;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    if (authentication) {
      http.csrf().disable().authorizeRequests()
          .antMatchers(HttpMethod.GET, "/**").permitAll()
          .antMatchers(HttpMethod.POST, "/**").authenticated()
          .and().httpBasic()
          .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    } else {
      http.csrf().disable().authorizeRequests()
          .antMatchers(HttpMethod.GET, "/**").permitAll()
          .antMatchers(HttpMethod.POST, "/**").permitAll()
          .and().httpBasic()
          .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth)
      throws Exception {
    auth.inMemoryAuthentication()
        .withUser("ibm_test_robot_1")
        .password("{noop}ibm_test_1.123#")
        .roles(DEFAULT_ROLE)
        .and()
        .withUser("ibm_test_robot_2")
        .password("{noop}ibm_test_2.123#")
        .roles(DEFAULT_ROLE)
        .and()
        .withUser("ibm_test_robot_3")
        .password("{noop}ibm_test_3.123#")
        .roles(DEFAULT_ROLE);
  }
}
