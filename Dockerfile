FROM maven:3.6-jdk-8-alpine
LABEL MAINTAINER="Cintya Aguirre cintya.aguirre@gmail.com"

VOLUME /tmp

RUN mkdir -p /var/app
COPY . /var/app
WORKDIR /var/app

ENTRYPOINT ["/var/app/docker-entrypoint.sh"]
