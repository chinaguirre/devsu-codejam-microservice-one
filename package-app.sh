#!/usr/bin/env bash
unset MAVEN_CONFIG # --> https://issues.jenkins-ci.org/browse/JENKINS-47890
./mvnw package
cp ./target/microserviceone-0.0.1-SNAPSHOT.jar microserviceone.jar